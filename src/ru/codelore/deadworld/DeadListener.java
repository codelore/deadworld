package ru.codelore.deadworld;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerRespawnEvent;

public class DeadListener implements Listener {
	
	@EventHandler
	public void onDeath(PlayerDeathEvent event) {
		Player p = event.getEntity();
		if(DeadWorld.deadPlayers.containsKey(p.getName())) {
			return;
		}
		DeadWorld.setDead(p);
	}
	
	@EventHandler
	public void onRespawn(PlayerRespawnEvent event) {
		Player p = event.getPlayer();
		if(DeadWorld.deadButNotRespawnedPlayers.contains(p.getName())) {
			DeadWorld.deadButNotRespawnedPlayers.remove(p.getName());
		}
	}
	
	@EventHandler
	public void onMove(PlayerMoveEvent event) {
		Player p = event.getPlayer();
		if(!(DeadWorld.deadPlayers.containsKey(p.getName()))) {
			return;
		}
		if(DeadWorld.deadButNotRespawnedPlayers.contains(p.getName())) {
			return;
		}
		if(event.getPlayer().getLocation().distanceSquared(DeadWorld.deadPlayers.get(p.getName()).get(0)) > Math.pow(6.0, 2)) {
			return;
		}
		DeadWorld.setUnDead(p);
	}
	
	@EventHandler
	public void onJoin(PlayerJoinEvent event) {
		Player p = event.getPlayer();
		if(DeadWorld.deadPlayers.containsKey(p.getName())) {
			for(Player plToHide : Bukkit.getOnlinePlayers()) {
				plToHide.hidePlayer(p);
				p.hidePlayer(plToHide);
			}
		} else {
			for(String plToHide : DeadWorld.deadPlayers.keySet()) {
				Player pl = Bukkit.getPlayer(plToHide);
				if(pl != null)
					p.hidePlayer(pl);
			}
		}
	}
	
	@EventHandler
	public void onBlockBreak(BlockBreakEvent event) {
		Player p = event.getPlayer();
		if(DeadWorld.deadPlayers.keySet().contains(p.getName())) {
			event.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onBlockPlace(BlockPlaceEvent event) {
		Player p = event.getPlayer();
		if(DeadWorld.deadPlayers.keySet().contains(p.getName())) {
			event.setBuild(false);
			event.setCancelled(true);
		}
	}
	
}
