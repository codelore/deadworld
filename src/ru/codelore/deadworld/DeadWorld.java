package ru.codelore.deadworld;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class DeadWorld extends JavaPlugin {
	
	public static HashMap<String, List<Location>> deadPlayers;
	public static List<String> deadButNotRespawnedPlayers;
	
	Logger log;
	
	public void onEnable() {
		deadPlayers = new HashMap<String, List<Location>>();
		deadButNotRespawnedPlayers = new ArrayList<String>();
		
		log = Logger.getLogger("Minecraft");
		PluginManager pm = getServer().getPluginManager();
		
		pm.registerEvents(new DeadListener(), this);
		
		getServer().getScheduler().scheduleAsyncRepeatingTask(this, new Runnable() {
			@Override
			public void run() {
				for(String pn : deadPlayers.keySet()) {
					Player p = Bukkit.getPlayer(pn);
					if(p != null) {
						p.setPlayerTime(15000, false);
						for(Location loc : DeadWorld.deadPlayers.get(p.getName())) {
							p.sendBlockChange(loc, Material.GOLD_BLOCK, (byte) 0);
						}
					}
				}
			}
		}, 20L, 20L);
	}
	
	public void onDisable() {
		
	}
	
	public static void setDead(Player p) {
		deadButNotRespawnedPlayers.add(p.getName());
		Location l = p.getLocation();
		World w = l.getWorld();
		int x = l.getBlockX();
		int y = l.getBlockY();
		int z = l.getBlockZ();
		List<Location> blocksToClick = new ArrayList<Location>();
		for(; y <= (l.getBlockY() + 3); y++) {
			if(y == (l.getBlockY() + 2)) {
				x--;
				blocksToClick.add(w.getBlockAt(x, y, z).getLocation());
				x++;
				blocksToClick.add(w.getBlockAt(x, y, z).getLocation());
				x++;
				blocksToClick.add(w.getBlockAt(x, y, z).getLocation());
				x = l.getBlockX();
			} else {
				blocksToClick.add(w.getBlockAt(x, y, z).getLocation());
			}
		}
		for(Player plToHide : Bukkit.getOnlinePlayers()) {
			plToHide.hidePlayer(p);
			p.hidePlayer(plToHide);
		}
		deadPlayers.put(p.getName(), blocksToClick);
	}
	
	public static void setUnDead(Player p) {
		p.resetPlayerTime();
		for(Player plToSee : Bukkit.getOnlinePlayers()) {
			if(deadPlayers.keySet().contains(plToSee)) {
				continue;
			}
			p.showPlayer(plToSee);
			plToSee.showPlayer(p);
		}
		for(Location loc : DeadWorld.deadPlayers.get(p.getName())) {
			p.sendBlockChange(loc, loc.getBlock().getType(), (byte) 0);
		}
		deadPlayers.remove(p.getName());
	}

}
